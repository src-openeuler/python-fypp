%global _empty_manifest_terminate_build 0
Name:		python-fypp
Version:	3.2
Release:	1
Summary:	Python powered Fortran preprocessor
License:	BSD-3-Clause
URL:		https://github.com/aradi/fypp
Source0:	https://files.pythonhosted.org/packages/01/35/0e2dfffc90201f17436d3416f8d5c8b00e2187e410ec899bb62cf2cea59b/fypp-3.2.tar.gz
BuildArch:	noarch


%description
Python powered Fortran preprocessor.

%package -n python3-fypp
Summary:	Python powered Fortran preprocessor
Provides:	python-fypp
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-fypp
Python powered Fortran preprocessor.

%package help
Summary:	Development documents and examples for fypp
Provides:	python3-fypp-doc
%description help
Python powered Fortran preprocessor.

%prep
%autosetup -n fypp-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-fypp -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Dec 06 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 3.2-1
- Update package to version 3.2

* Wed May 11 2022 houyingchao <houyingchao@h-partners.com> - 3.1-2
- License compliance rectification

* Mon Sep 06 2021 Python_Bot <Python_Bot@openeuler.org> - 3.1-1
- Package Init
